﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsoMCcontroller : MonoBehaviour
{
    public float movementSpeed = 1f;
    IsoMCchararend isoRenderer;

    Rigidbody2D rbody;


    private void Awake()
    {
        rbody = GetComponent<Rigidbody2D>();
        isoRenderer = GetComponentInChildren<IsoMCchararend>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 currentPos = rbody.position; //store character position based on their rigidbody2d

        //to move character:
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");


        Vector2 inputVector = new Vector2(horizontalInput, verticalInput); //store these variables in...

        

        inputVector = Vector2.ClampMagnitude(inputVector, 1); //to limit the magnitude of, to prevent diagonal movement from becoming faster than movement in the cardinal directions


        Vector2 movement = inputVector * movementSpeed;
        Vector2 newPos = currentPos + movement * Time.fixedDeltaTime;

       


        isoRenderer.SetDirection(movement);
        rbody.MovePosition(newPos);
    }

}
