﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using UnityEngine;

public class ScriptBottone : MonoBehaviour
{
    public GameObject Door1;
    public GameObject Door2;
    public GameObject Door3;
    private bool attivo1 = false;
    private bool attivo2 = false;
    private bool attivo3 = false;

    private void Start()
    {
        attivo1 = Door1.activeSelf;
        attivo2 = Door2.activeSelf;
        attivo3 = Door3.activeSelf;

    }

    IEnumerator Controllo()
    {
        attivo1 = Door1.activeSelf;
        attivo2 = Door2.activeSelf;
        attivo3 = Door3.activeSelf;
        if (attivo1 == true) { 
            Door1.SetActive(false);

        }
        if (attivo1 == false)
        {
            Door1.SetActive(true);
            
        }
        if (attivo2 == true)
        {
            Door2.SetActive(false);
            
        }
        if (attivo2 == false)
        {
            Door2.SetActive(true);
            
        }
        if (attivo3 == true)
        {
            Door3.SetActive(false);
            
        }
        if (attivo3 == false)
        {
            Door3.SetActive(true);

        }
        yield return null;
    }


}
