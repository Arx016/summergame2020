﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Dialog: MonoBehaviour{

    
    public GameObject ManagerDialogo;
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    private int index;
    public float typingSpeed;
    public GameObject Player;
    public IsoMCcontroller ComponentPlayer;
    public GameObject bottoneAvanti;
        void Start()
    {
         StartCoroutine(Type());
        
    }
        void Update()
    {
        if(textDisplay.text == sentences[index])
        {
            bottoneAvanti.SetActive(true);
        }
    }

    IEnumerator Type(){
        /*Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;*/
        foreach (char letter in sentences[index].ToCharArray()){

            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);

        }
    }

    public void FraseDopo(){

        bottoneAvanti.SetActive(false);

        if (index < sentences.Length-1){
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }else 
        {
            /*Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;*/
            ComponentPlayer.enabled=true;
            ManagerDialogo.SetActive(false);
        }
    
    }


}
