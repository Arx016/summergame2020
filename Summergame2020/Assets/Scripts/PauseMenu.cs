﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GamePaused = false;

    public GameObject menuPausaUI;
    public GameObject menuOpzioniUI;

    void Start()
    {
        //Set Cursor to not be visible
        //Cursor.visible = false;
    }

    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.P)) {

            if (GamePaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    


    public void Resume()
    {
        menuPausaUI.SetActive(false);
        Time.timeScale = 1f;
        GamePaused = false;
        //Cursor not visible and locked
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
    }

    void Pause()
    {
        menuPausaUI.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;
        //cursor visible but confined
        /*Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;*/
    }

    public void MenuIniz()
    {
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }

    public void optionMenu()
    {
        menuOpzioniUI.SetActive(true);
        menuPausaUI.SetActive(false);
    }


}
